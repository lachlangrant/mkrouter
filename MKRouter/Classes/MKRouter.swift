//
//  MKRouter.swift
//  MKRouter
//
//  Created by Lachlan Grant on 24/1/18.
//

import UIKit

public class MKRouter {
    public static let `default`:MKIsRouter = MKDefaultRouter()
}

public protocol MKNavigation { }

public protocol MKAppNavigation {
    func viewcontrollerForNavigation(navigation: MKNavigation) -> UIViewController
    func navigate(_ navigation: MKNavigation, from: UIViewController, to: UIViewController)
}

public protocol MKIsRouter {
    func setupAppNavigation(appNavigation: MKAppNavigation)
    func navigate(_ navigation: MKNavigation, from: UIViewController)
    func didNavigate(block: @escaping (MKNavigation) -> Void)
    var appNavigation: MKAppNavigation? { get }
}

public extension UIViewController {
    public func navigate(_ navigation: MKNavigation) {
        MKRouter.default.navigate(navigation, from: self)
    }
}

public class MKDefaultRouter: MKIsRouter {
    
    public var appNavigation: MKAppNavigation?
    var didNavigateBlocks = [((MKNavigation) -> Void)] ()
    
    public func setupAppNavigation(appNavigation: MKAppNavigation) {
        self.appNavigation = appNavigation
    }
    
    public func navigate(_ navigation: MKNavigation, from: UIViewController) {
        if let toVC = appNavigation?.viewcontrollerForNavigation(navigation: navigation) {
            appNavigation?.navigate(navigation, from: from, to: toVC)
            for b in didNavigateBlocks {
                b(navigation)
            }
        }
    }
    
    public func didNavigate(block: @escaping (MKNavigation) -> Void) {
        didNavigateBlocks.append(block)
    }
}

// Injection helper
public protocol Initializable { init() }
open class RuntimeInjectable: NSObject, Initializable {
    public required override init() {}
}

public func appNavigationFromString(_ appNavigationClassString: String) -> MKAppNavigation {
    let appNavClass = NSClassFromString(appNavigationClassString) as! RuntimeInjectable.Type
    let appNav = appNavClass.init()
    return appNav as! MKAppNavigation
}
