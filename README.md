# MKRouter

[![CI Status](http://img.shields.io/travis/lachlangrant/MKRouter.svg?style=flat)](https://travis-ci.org/lachlangrant/MKRouter)
[![Version](https://img.shields.io/cocoapods/v/MKRouter.svg?style=flat)](http://cocoapods.org/pods/MKRouter)
[![License](https://img.shields.io/cocoapods/l/MKRouter.svg?style=flat)](http://cocoapods.org/pods/MKRouter)
[![Platform](https://img.shields.io/cocoapods/p/MKRouter.svg?style=flat)](http://cocoapods.org/pods/MKRouter)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MKRouter is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MKRouter'
```

## Author

lachlangrant, lachlangrant@rbvea.co

## License

MKRouter is available under the MIT license. See the LICENSE file for more info.
